# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 05:19:16 2020

@author: jonis
"""

import codecs
from random import shuffle

def person_names(x):
    all_names = []
    with codecs.open("all_names.txt", encoding='utf-8') as f:
        for line in f:
            all_names.extend(line.split())
    shuffle(all_names)
    names = []
    for _ in range(x):
        names.append(all_names.pop())
    return names