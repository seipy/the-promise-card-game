# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 14:22:04 2020

@author: jonis
"""


'''
    context information:
        
        use: data = context[attribute]
        example: promise = context["legal_promises"][0]
        
        attributes:
            player_count:       int - player count
            pos:                int - the player's position
            pos_from_dealer:    int - the distance between the dealer and the player; if the player is the dealer, it is 0
            round:              int - round count, starts from 1
            turn:               int - turn count since last tick, starts from 1
            points:             int - the player's points
            ticks:              int - the player's ticks
            suit:               int - the asked suit of the turn; it is -1 if it has not been set yet
            greatest_value:     int - the greatest value of any card in the current_table; it is -1 if the current_table is empty
            legal_play_indexes: list(int) - the indexes of the cards the player is allowed to play
            legal_promises:     list(int) - list of possible promises the player is allowed to promise
            hand:               list(Card) - the player's cards in hand
            legal_plays:        list(Card) - the cards the player is allowed to play
            current_table:      list(Card) - list of active cards on the table
            players_all:        list(dict("points","ticks","promise","table","type")) - all global information available of the players; elements of the list are entries to players' information
'''


ais = dict()

class BaselineAI:
    def __init__(self):
        pass
    def get_promise(self, context):
        promise = context["legal_promises"][0]
        return promise
    def get_play(self, context):
        play = context["legal_play_indexes"][0]
        return play
ais["baseline"] = BaselineAI()


class CowardAI:
    def __init__(self):
        pass
    def get_promise(self, context):
        promise = context["legal_promises"][0]
        return promise
    def get_play(self, context):
        i = -1
        playable_cards = context["legal_plays"]
        if context["suit"] not in [card.suit for card in context["legal_plays"]]+[-1]:
            max_val = max([card.value for card in playable_cards])
            i = [card.value for card in playable_cards].index(max_val)
        elif context["greatest_value"] > playable_cards[0].value:
            best_val = max(list(filter(lambda card : card.value < context["greatest_value"], playable_cards))).value
            i = [card.value for card in playable_cards].index(best_val)
        else:
            min_val = min([card.value for card in playable_cards])
            i = [card.value for card in playable_cards].index(min_val)
        
        if i in context["legal_play_indexes"]:
            play = i
        else:
            play = context["legal_play_indexes"][0]
        return play
ais["coward"] = CowardAI()


class SmartAI:
    def __init__(self):
        pass
    def get_promise(self, context):
        promise = context["legal_promises"][0]
        return promise
    def get_play(self, context):
        play = context["legal_play_indexes"][0]
        return play
ais["smart"] = SmartAI()


def manager(ai_type):
    try:
        return ais[ai_type]
    except KeyError:
        print("WARNING: Requested AI (%s) does not match any existing AIs. Returning BaselineAI" % ai_type)
        return BaselineAI()