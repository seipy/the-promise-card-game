# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 18:56:06 2020

@author: jonis
"""

import json

from random import shuffle

from person_names import person_names
import ais

class Player:
    def __init__(self, position, name="NoName", p_type=0, p_ai=None):
        self.position = position
        self.name = name
        self.p_type = p_type # 0 = human; 1 = AI
        self.p_ai = p_ai
        self.cards_hand = []
        self.cards_table = []
        self.promise = 0
        self.ticks = 0
        self.points = 0
    
    def __lt__(self, other):
        return self.points < other.points
        
        
class Card:
    def __init__(self, value, suit):
        self.value = value
        self.suit = suit
        self.owner = -1
        
    def __lt__(self, other):
        if self.suit < other.suit:
            return True
        elif self.suit > other.suit:
            return False
        else:
            if self.value < other.value:
                return True
            elif self.value > other.value:
                return False
            else:
                assert False, "Duplicate cards detected, but the cards should be unique!"


class Deck:
    def __init__(self):
        self.cards = []
        self.fill_deck()
        
    def fill_deck(self):
        self.cards = []
        card_values = list(range(13))
        card_suits = list(range(4))
        for suit in card_suits:
            for value in card_values:
                self.cards.append(Card(value,suit))
        shuffle(self.cards)
                
    def give_card(self, owner):
        self.cards[-1].owner = owner
        return self.cards.pop()


class GameEngine:
    def __init__(self, player_count, rounds=-1, players_meta=[], hide_others=False):
        '''rounds = -1: the game determines optimal amount of rounds
           rounds = -2: the game forces maximum amount of rounds for the player count
        '''
        self.game_on = True
        self.round_count = 1
        self.players = []
        for i in range(player_count):
            if not players_meta:
                player_names = person_names(player_count)
                self.players.append(Player(i, player_names.pop(), 0))
            else:
                player_name = players_meta[i]["name"]
                player_type = players_meta[i]["ai"]
                if player_type == "False":
                    player_type = 0
                    player_ai = None
                else:
                    player_ai = ais.manager(player_type)
                    player_type = 1
                player = Player(i, player_name, player_type, player_ai)
                self.players.append(player)
        self.deck = Deck()
        self.dealer_position = 0
        
        if rounds in [-1, -2]:
            self.rounds = len(self.deck.cards) // player_count
            if rounds == -1 and self.rounds > 10:
                self.rounds = 10
        else:
            assert rounds > 0, "The amount of rounds must be a positive integer, or one of the predefined negative integers for fixed settings"
            if len(self.deck.cards) < rounds * player_count:
                rounds = len(self.deck.cards) // player_count
            self.rounds = rounds
        self.turn = 1
        self.turn_suit = -1
        self.current_table = []
        self.hide_others = hide_others
    
    def init_round(self):
        self.deck.fill_deck()
        for player in self.players:
            self.init_cards(player)
            player.ticks = 0
            player.promise = 0
        print("Round %i starts!" % self.round_count)
            
    def process_round(self):
        self.init_round()
        self.process_promises()
        self.play_round()
        self.scoring()
        if self.rounds == 1:
            self.game_end()
        else:
            self.rounds -= 1
            self.dealer_position += 1
            if self.dealer_position >= len(self.players):
                self.dealer_position = 0
        self.round_count += 1
    
    def init_cards(self, player):
        player.cards_hand, player.cards_table = [],[]
        for _ in range(self.rounds):
            player.cards_hand.append(self.deck.give_card(player.position))
        player.cards_hand = sorted(player.cards_hand)
            
    def process_promises(self):
        for player in self.players:
            if player.p_type == 0:
                promise_flag = False
                while not promise_flag:
                    self.print_game(player)
                    player.promise = input("%s, make a promise: " % player.name)
                    try:
                        player.promise = int(player.promise)
                        assert 0 <= player.promise <= self.rounds
                    except (ValueError, AssertionError):
                        print("Please provide an integer on the range from 0 to %i" % self.rounds)
                        player.promise = 0
                    else:
                        promise_flag = True
            else:
                player.promise = player.p_ai.get_promise(self.create_context(player))
                print("AI makes a promise: %i" % player.promise)
        print()
        
    def play_round(self):
        max_promise = max(player.promise for player in self.players)
        max_positions = [player.position for player in self.players if player.promise == max_promise]
        
        start_pos = self.dealer_position
        while start_pos not in max_positions:
            start_pos += 1
            if start_pos == len(self.players):
                start_pos = 0
        curr_pos = start_pos
        player = self.players[curr_pos]
        print("%s, you start." % player.name)
        
        round_flag = False
        while not round_flag:
            start_pos = curr_pos
            self.turn_suit = -1
            turn_flag = False
            while not turn_flag:
                legal_plays = self.get_legal_plays(player.cards_hand)
                self.print_options(legal_plays)
                
                p_cards_amount = len(legal_plays)-1
                choice_flag = False
                while not choice_flag:
                    if player.p_type == 0:
                        play_choice = input("Select a card to play: ")
                    else:
                        play_choice = player.p_ai.get_play(self.create_context(player, legal_plays))
                    try:
                        play_choice = int(play_choice)
                        assert 0 <= play_choice <= p_cards_amount
                    except (ValueError, AssertionError):
                        print("Please provide an integer on the range from 0 to %i" % p_cards_amount)
                    else:
                        choice_flag = True
                print("\n")
                
                card = legal_plays.pop(play_choice)
                try:
                    player.cards_hand.remove(card)
                    # pop from legal_plays removes card from the first player's hand, but not the other players', thus this solution
                except:
                    pass
                
                if self.turn_suit == -1:
                    self.turn_suit = card.suit
                player.cards_table.append(card)
                self.current_table.append(card)
                
                curr_pos += 1
                if curr_pos == len(self.players):
                    curr_pos = 0
                if curr_pos == start_pos:
                    turn_flag = True
                    curr_pos = self.give_tick()
                
                player = self.players[curr_pos]
                self.print_game(player)
                
                if len(player.cards_hand) != 0:
                    print("%s, you go next." % player.name)
                    self.turn += 1
                else:
                    round_flag = True
                    self.turn = 1
    
    def get_legal_plays(self, cards):
        if self.turn_suit == -1 or self.turn_suit not in [card.suit for card in cards]:
            return cards
        else:
            return list(filter(lambda card : card.suit == self.turn_suit, cards))
    
    def give_tick(self):
        winning_card = sorted(filter(lambda card : card.suit == self.turn_suit,[player.cards_table[-1] for player in self.players]))[-1]
        winner = self.players[winning_card.owner]
        winner.ticks += 1
        self.current_table = []
        
        print("%s wins a tick with " % winner.name, end="")
        self.print_card(winning_card)
        print("\n")
        
        return winner.position
    
    def scoring(self):
        print("Round %i over! Players got the following points: " % self.round_count)
        for player in self.players:
            increment = 0
            if player.ticks == player.promise:
                increment = int("1"+str(player.promise))
                player.points += increment
            print("%s: %i" % (player.name, increment))
        print()
    
    def game_end(self):
        self.game_on = False
        self.print_standings()
        
    def create_context(self, player_in_turn, legal_plays=[]):
        c = dict()
        c["player_count"] = len(self.players)
        c["pos"] = player_in_turn.position
        pos_from_dealer = player_in_turn.position - self.dealer_position
        if pos_from_dealer < 0:
            pos_from_dealer += len(self.players)
        c["pos_from_dealer"] = pos_from_dealer
        c["round"] = self.round_count
        c["turn"] = self.turn
        c["points"] = player_in_turn.points
        c["ticks"] = player_in_turn.ticks
        c["hand"] = player_in_turn.cards_hand
        c["legal_plays"] = legal_plays
        c["legal_play_indexes"] = list(range(len(legal_plays)))
        c["legal_promises"] = list(range(len(player_in_turn.cards_hand)))
        players_all = []
        for player in self.players:
            player_all = {"points":player.points, "ticks":player.ticks, "promise":player.promise, "table":player.cards_table, "type":player.p_type}
            players_all.append(player_all)
        c["players_all"] = players_all
        c["current_table"] = self.current_table
        c["suit"] = self.turn_suit
        values_list = list(filter(lambda card : card.suit == self.turn_suit, self.current_table))
        if not values_list:
            greatest_value = -1
        else:
            greatest_value = max(values_list).value
        c["greatest_value"] = greatest_value
        
        return c
    
    def value2str(self,i):
        values = ["2","3","4","5","6","7","8","9","T","J","Q","K","A"]
        return values[i]
    
    def suit2str(self,i):
        symbols = ["♥","♧","♢","♤"]
        return symbols[i]
    
    def print_card(self, card, option_index=-1):
        if option_index == -1:
            print("%s%s " % (self.value2str(card.value), self.suit2str(card.suit)), end="")
        else:
            print("%i: %s%s " % (option_index, self.value2str(card.value), self.suit2str(card.suit)), end="")
    
    def print_game(self, player_in_turn):
        print("Dealer: %s\n" % self.players[self.dealer_position].name)
        for player in self.players:
            print("Cards of %s (pts: %i):" % (player.name, player.points))
            if player == player_in_turn or not self.hide_others:
                for card in player.cards_hand:
                    self.print_card(card)
                print()
            else:
                print("[hidden]")
            
            print("Cards played (won %i, promised %i): " % (player.ticks, player.promise))
            for card in player.cards_table:
                self.print_card(card)
            print("\n")
            
    def print_options(self, legal_plays):
        for i,card in enumerate(legal_plays):
            self.print_card(card, i)
            
    def print_standings(self):
        print("Game over! Standings: ")
        self.players = sorted(self.players, reverse=True)
        i = 1
        j = 1
        points = self.players[0].points
        for player in self.players:
            if player.points < points:
                i=j
                points = player.points
            j+=1
            print("%i. %s, %i pts" % (i, player.name, player.points))


player_count, rounds, players_meta, hide_others = -1, -1, [], False

try:
    with open("settings.json") as f:
        settings = json.load(f)
except FileNotFoundError:
    print("Settings file not found, using default settings...")
else:
    player_count = settings["players_meta"]["player_count"]
    rounds = settings["game_meta"]["rounds"]
    if settings["players_meta"]["random_players"] == "False":
        players_meta = settings["players_meta"]["players"]
    if settings["game_meta"]["hide_others"] == "True":
        hide_others = True

if player_count == -1:
    player_count = input("Give the number of players: ")

p_min, p_max = 2, 52
try:
    player_count = int(player_count)
    assert p_min <= player_count <= p_max
except (ValueError, AssertionError):
    assert False, "PLayer count must be between %i and %i" % (p_min, p_max)
    
ge = GameEngine(player_count, rounds, players_meta, hide_others)
while ge.game_on:
    ge.process_round()
